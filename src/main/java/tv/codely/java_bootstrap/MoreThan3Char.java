package tv.codely.java_bootstrap;

public class MoreThan3Char implements Filter{

    @Override
    public Boolean matches(Word word) {
        return word.length() > 3;
    }
}
