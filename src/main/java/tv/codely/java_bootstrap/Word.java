package tv.codely.java_bootstrap;

public class Word {

    final private String word;


    Word(String word){
        this.word = word;
    }



    public Boolean hasMoreThan3Characters() {
        return word.length() > 3;
    }

    public Boolean StartWithaVowel(){
        return word.startsWith("a") || word.startsWith("e") || word.startsWith("i")
         || word.startsWith("u") || word.startsWith("o") || word.startsWith("y");
    }

    public Boolean StartWithaVowelOrHasMoreThan3Characters(){
        return hasMoreThan3Characters() || (StartWithaVowel());
    }

    public Boolean StartWithaVowelANDHasMoreThan3Characters(){
        return hasMoreThan3Characters() && (StartWithaVowel());
    }

    public Boolean DontStartWithaVowelButHasMoreThan3Characters(){
        return hasMoreThan3Characters() && !(StartWithaVowel());
    }

    public int length() {
        return word.length();
    }

    public boolean startsWith(String a) {
        return word.startsWith(a);
    }
}
