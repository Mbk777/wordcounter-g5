package tv.codely.java_bootstrap;

public class StartWithaVowel implements Filter {
    @Override
    public Boolean matches(Word word) {
        return word.startsWith("a") || word.startsWith("e") || word.startsWith("i")
                || word.startsWith("u") || word.startsWith("o") || word.startsWith("y");
    }
}
