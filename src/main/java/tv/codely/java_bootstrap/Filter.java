package tv.codely.java_bootstrap;

public interface Filter {

    public Boolean matches(Word  word);

}
